! ********************************************************************* !
! Module for random number generation
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


MODULE random_generator

USE mt19937
IMPLICIT NONE
INTEGER                 :: random_counter
PUBLIC                  :: unirand,gausrand


CONTAINS


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Random counter initialisation routine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE initialize_random_generator(seed)

IMPLICIT NONE
INTEGER                 :: seed

CALL init_genrand(seed)
random_counter=0

END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Generate a random number from a uniform distribution
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION unirand() RESULT(value)

IMPLICIT NONE
DOUBLE PRECISION        :: value

value=grnd()
random_counter=random_counter+1

RETURN
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Generate a random number from a Gaussian distribution
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION gausrand() RESULT(value)

IMPLICIT NONE
DOUBLE PRECISION        :: value

value=GASDEV()

RETURN
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Random number generator for a Gaussian distribution
! (adapted from Numerical Recipes)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION GASDEV()
      
DOUBLE PRECISION        :: GASDEV
DOUBLE PRECISION        :: v1,v2,r,fac
      
!IF (idum.lt.0) iset=0
10 v1=2*unirand()-1
v2=2*unirand()-1

r=v1**2+v2**2

IF(r.ge.1.or.r.eq.0) GOTO 10

fac=sqrt(-2*log(r)/r)

GASDEV=v2*fac

RETURN
END FUNCTION

END MODULE